"""
    Train Natural Language Understanding (NLU)
        - Intent Analysis

    Author : Sangkeun Jung (hugmanskj@gmail.com), 2019
"""


# in this code, 
#   intent alaysis and domain_entity analysis implemented seperatley. 
#   (NOT JOINT MODEL for education purpose )
import os, codecs
from common.ml.hparams import HParams

# set current SCRIPT file directory as a working directory
os.chdir( os.path.dirname( os.path.abspath(__file__) ) )

from domains.common.utils.gpu import is_gpu_available

from domains.common.rsc_processor.n21 import load_rsc, make_examples
from domains.common.rsc_processor.n21 import load_text_rsc, load_n21_predicted_result_format

from domains.common.rsc_processor.vocabs import load_vocabs
from domains.common.utils.predict import load_model

from domains.common.network.nn import lstm_n21 as network
from domains.common.utils.train import train_n21 as train
from domains.common.utils.predict import test_n21 as test
# --------------------------------------------------------------------------------------


def run_train_intent(train_fn, hps, model_fn):
    # run nlu trainer (intent)
    # input : converted id format (train.id.n21.txt)
    # output : model file 
    
    train_rsc = load_rsc(train_fn)
    train_exs = make_examples(train_rsc, hps.num_steps)

    model = network(hps)
    train(model, train_exs, hps, model_fn)


def run_eval_intent(test_fn, model_fn, to_result_fn):
    # run nlu evaluator (intent)
    # input : test.id.txt, model
    # ouput : result.id.n21.txt
    
    model = load_model(model_fn)

    test_rsc = load_rsc(test_fn)
    test_exs = make_examples(test_rsc, model.hps.num_steps)

    test(model, test_exs, to_result_fn)


def convert_result_format(text_n2n_fn, n2n_result_fn, text_n21_result_fn, vocab):
    # input : 
    #   - text tokenized n21 format, 
    #   - n21_result_fn(only id and probability) (predicted), 
    #   - readable n21 text format (predicted)
    #   - vocab : n21 class symbol vocab

    reverse_vocab = { v: k for k, v in vocab.items() } 

    text_rsc = load_text_rsc(text_n2n_fn)
    predicted_rsc = load_n21_predicted_result_format(n2n_result_fn)

    if len(text_rsc) != len(predicted_rsc):
        print("ERROR: Reference({}) and Predicted({}) results do not match!!".format(len(text_rsc), len(predicted_rsc)) )
        import sys; sys.exit()

    # store the readable output (text_n21_result_fn)
    conv_result = []
    for sent_idx, (text_sent, id_sent) in enumerate( zip(text_rsc, predicted_rsc) ):

        if len(text_sent[1]) != len(id_sent[3]):
            print("ERROR: Sentence lengths do not match at sentence {} : Reference({}) vs. Predicted({})".format(sent_idx, len(text_sent[1]), len(id_sent[3])) )
            import sys; sys.exit()
        
        
        ref_class_id = int(id_sent[0])
        pred_class_id = int(id_sent[1])
        pred_prob = float(id_sent[2])

        ref_class  = reverse_vocab[ref_class_id]
        pred_class = reverse_vocab[pred_class_id]
        tokens = text_sent[1] 

        _diff = 'O' if ref_class == pred_class else 'X' 
        conv_result.append( (_diff, ref_class, pred_class, pred_prob, tokens) )

    # dump
    with codecs.open(text_n21_result_fn, 'w', encoding='utf-8') as f:
        for sent_number, (_diff, ref_class, pred_class, pred_prob, tokens) in enumerate( conv_result ):
            print("[{}]\t{}\t{}\t{:5.3f}\t{}".format(
                                                _diff,
                                                ref_class,
                                                pred_class,
                                                pred_prob,
                                                "\t".join(tokens)
                                            ),
                    file=f
                    )
        print("Readable N21 result format is dumped at {}".format(text_n21_result_fn) )



import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--mode", help="mode - train, eval", default="train")
#parser.add_argument("--mode", help="mode - train, eval", default="eval")
#parser.add_argument("--mode", help="mode - train, eval", default="pred")

parser.add_argument("--gpu_id", help="gpu device id", default="0")
parser.add_argument("--model_key", help=" model identifier", default="default")

#parser.add_argument("--use_gpu", help="use gpu=True or False", default=True)
parser.add_argument("--use_gpu", help="use gpu=True or False", default=False)


parser.add_argument("--keep_prob", help="keep prob. for dropout", default=0.5)           # Kp
parser.add_argument("--token_emb_dim", help="token_emb_dim", default=50, type=int)       # T=
parser.add_argument("--intent_dim", help="intent_dim", default=50, type=int)             # Ie=

parser.add_argument("--domain", help="domain name", default="weather")             # dm

args = parser.parse_args()
os.environ ["CUDA_VISIBLE_DEVICES"] = args.gpu_id

if not is_gpu_available(): args.use_gpu = False

if __name__ == '__main__':
    domain = args.domain 

    data_root = '../data'
    model_root = '../model'

    text_data_folder     = os.path.join(data_root, domain, "tokenized")
    id_data_folder       = os.path.join(data_root, domain, "id_converted")
    vocab_folder         = os.path.join(data_root, domain, "vocab")

    model_dir       = os.path.join(model_root, domain, args.model_key)

    fns = {
            'text' : {
                        'train' : os.path.join(text_data_folder, 'train.text.n21.txt'),
                        'test' : os.path.join(text_data_folder, 'test.text.n21.txt'),
                    },

            'train' : os.path.join(id_data_folder, 'train.id.n21.txt'),
            'test'  : os.path.join(id_data_folder, 'test.id.n21.txt'),
            'vocab' : {
                        'token' : os.path.join(vocab_folder, 'token.vocab'),
                        'n21' : os.path.join(vocab_folder, 'n21.class.voab'),
                        'n2n' : os.path.join(vocab_folder, 'n2n.class.voab'),
                },
            'model' : os.path.join(model_dir, 'model.n21.out'),
            'result_fn' : os.path.join(model_dir, 'result.n21.id.txt'),
            'result_fn_n21' : os.path.join(model_dir, 'result.n21.text.txt'),
          }

    vocabs = load_vocabs(fns['vocab']) # key = token or n21 or n2n 

    if args.mode == 'train':
        hps= HParams(
                            ## common
                            batch_size        = 100,
                            num_steps         = 64,  # max token numbers in a sentence

                            learning_rate     = 0.01,
                            keep_prob         = float(args.keep_prob),  # 1.0 for evaluation & prediction
                            max_epoch         = 50,
                            use_gpu           = args.use_gpu,

                            # encoder
                            token_emb_dim     = args.token_emb_dim,
                            text_encoder_num_layers = 1,
                            intent_dim =  args.intent_dim,

                            # vocab 
                            num_token_vocabs  = len( vocabs['token'] ),
                            num_intent_tags   = len( vocabs['n21']),
                        )

        if not os.path.exists(model_dir): os.makedirs(model_dir)
        run_train_intent(fns['train'], hps, fns['model'])
        run_eval_intent(fns['test'], fns['model'], fns['result_fn'])
        convert_result_format(fns['text']['test'], fns['result_fn'], fns['result_fn_n21'], vocabs['n21'])

    if args.mode == 'eval':
        run_eval_intent(fns['test'], fns['model'], fns['result_fn'])
        convert_result_format(fns['text']['test'], fns['result_fn'], fns['result_fn_n21'], vocabs['n21'])

    if args.mode == 'pred':
        from domains.common.utils.predict import Predictor_N21
        predictor = Predictor_N21(fns['model'], vocabs['token'], vocabs['n21'], device='cpu')
        predicted_intent, predicted_prob = predictor.predict("오늘 날씨 어떻게 되지?")
        print(predicted_intent, predicted_prob)

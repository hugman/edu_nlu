"""
    Test Utils (Common)

    Author : Sangkeun Jung (hugmanskj@gmail.com), 2019
"""
import torch 
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import numpy as np 

from torch.utils.data import (DataLoader, RandomSampler, SequentialSampler, TensorDataset)

def load_model(model_fn, map_location=None):

    # if cuda is not available?? force to load on cpu

    
    if map_location:
        return torch.load(model_fn, map_location=map_location)
    else:
        if torch.cuda.is_available():
            return torch.load(model_fn)
        else:
            return torch.load(model_fn, map_location='cpu')

import codecs
def test_n21(model, test_exs, to_result_fn):
    model.eval() # set information to pytorch that the current mode is 'testing'

    # use gpu if set
    device = torch.device("cpu") # default
    if torch.cuda.is_available():
        if model.hps.use_gpu:  
            device = torch.device("cuda")
    if device.type == 'cuda': model = model.cuda()

    all_predicts   = []
    all_references = []
    all_probs      = []
    all_tokens     = []
    all_weights    = []

    # ---- dataset ---- #
    all_label_ids = torch.tensor([f.label_id for f in test_exs], dtype=torch.long)
    all_token_ids = torch.tensor([f.token_ids for f in test_exs], dtype=torch.long)
    all_weight    = torch.tensor([f.weight for f in test_exs], dtype=torch.float)

    test_data = TensorDataset(all_label_ids, all_token_ids, all_weight)
    test_sampler = SequentialSampler(test_data)
    test_dataloader = DataLoader(test_data, sampler=test_sampler, batch_size=model.hps.batch_size)

    for step, batch in enumerate(test_dataloader):
        batch = tuple(t.to(device) for t in batch)
        label_id, token_ids, weight = batch
        predicts  = model(token_ids, weight, mode='eval')
        np_probs  = F.softmax(predicts, dim=1).data.cpu().numpy() # to get probability
        pred_idxs = np.argmax(np_probs, axis=1)

        for b_idx, p in enumerate(pred_idxs): 
            all_predicts.append(p)
            all_probs.append( np_probs[b_idx][p] )

        for r in label_id.data.numpy():    all_references.append(r)

        for t in token_ids.data.numpy(): all_tokens.append(t)
        for w in weight.data.numpy():  all_weights.append(w)

    # dump 
    # calculate the accuracy
    num_corrects = 0
    with codecs.open(to_result_fn, 'w', encoding='utf-8') as f:
        for pred, ref, prob, token, weight in zip(all_predicts, all_references, all_probs, all_tokens, all_weights):
            if pred == ref : num_corrects += 1

            # to get non-padding symbol 
            # token * weight for making padding symbol id as zero
            r_tokens = list(token * weight)
            r_tokens.reverse()
            token_ids = [ str(t_id) for t_id in r_tokens if t_id > 0 ] 
            
            # ref \t pred \t prob \t token ids 
            print("{}\t{}\t{}\t{}".format(
                                            ref,
                                            pred,
                                            prob,
                                            "\t".join(token_ids)
                                            )
                  , file=f
                  )
    print("Evaluation file(id-based) is dumped at ", to_result_fn)

    accuracy = float(num_corrects) / float( len(all_predicts) )
    print("Accuracy of the model on testing data : {:.6f}".format(accuracy))

def test_n2n(model, test_exs, to_result_fn, actual_num_exs):
    model.eval() # set information to pytorch that the current mode is 'testing'
    device = torch.device("cuda" if model.hps.use_gpu else "cpu")


    # ---- dataset ---- #
    all_token_ids = torch.tensor([f.token_ids for f in test_exs], dtype=torch.long)
    all_weight    = torch.tensor([f.weight for f in test_exs], dtype=torch.float)
    all_tag_ids   = torch.tensor([f.tag_ids for f in test_exs], dtype=torch.long)

    test_data       = TensorDataset(all_token_ids, all_weight, all_tag_ids)
    test_sampler    = SequentialSampler(test_data)
    test_dataloader = DataLoader(test_data, sampler=test_sampler, batch_size=model.hps.batch_size)

    result_lines = []
    for step, batch in enumerate(test_dataloader):
        batch = tuple(t.to(device) for t in batch)
        token_ids, weight, tag_ids = batch
        ref_target_ids = tag_ids

        # feed-forward (inference)
        logits = model(token_ids, weight, mode='eval')  # logits = [batch_size, num_steps, num_target_class]

        # for simplicity 
        step_result = []
        for idx, a_sent in enumerate(logits):
            for step_logit in a_sent: # a_sent : [num_steps, num_target_class]
                step_probs = F.softmax(step_logit)
                values, indices = torch.max(step_probs, 0)

                best_index = int( indices.data.cpu().numpy() )
                best_prob  = float( values.data.cpu().numpy() )
                
                step_result.append( (best_index, best_prob ) )

            # to result format
            weights = weight[idx]
            items = []
            for t_idx, _weight in enumerate( weights ):
                if _weight != 1: continue 
                best_class_index, best_class_prob = step_result[t_idx]
                item = "{},{}".format(best_class_index, best_class_prob)
                items.append( item )                
            a_line = "\t".join( items ) 
            result_lines.append( a_line )

    # ignore randomly filled exs 
    with codecs.open(to_result_fn, 'w', encoding='utf-8') as f:
        for line in result_lines:
            print(line, file=f)

    print("Prediction result is dumped at {}".format(to_result_fn) )


def test_crf_n2n(model, test_exs, to_result_fn, fast_eval=True):
    ## fast_eval == True ? --> fast decoding with batch_size = large --> not correct prob
    ## fast_eval == False  --> slow decoding with batch_size = 1 --> correct sequence-wise prob
    ## this is to do list. 

    model.eval() # set for evluation mode
    if fast_eval == False:
        batch_size = 1
    else:
        batch_size=model.hps.batch_size

    device = torch.device("cuda" if model.hps.use_gpu else "cpu")

    # ---- dataset ---- #
    all_token_ids = torch.tensor([f.token_ids for f in test_exs], dtype=torch.long)
    all_weight    = torch.tensor([f.weight for f in test_exs], dtype=torch.float)
    all_tag_ids   = torch.tensor([f.tag_ids for f in test_exs], dtype=torch.long)

    test_data       = TensorDataset(all_token_ids, all_weight, all_tag_ids)
    test_sampler    = SequentialSampler(test_data)
    test_dataloader = DataLoader(test_data, sampler=test_sampler, batch_size=batch_size)

    result_lines = []
    for step, batch in enumerate(test_dataloader):
        batch = tuple(t.to(device) for t in batch)
        token_ids, weight, tag_ids = batch
        ref_target_ids = tag_ids

        # feed-forward (inference)
        logits = model(token_ids)  # logits = [batch_size, num_steps, num_target_class]

        # pred_tag_ids : a list of list ( list for masked tag ids ) <- BATCH_SIZE
        # prob_paths   : a numpy list path prob <- BATCH_SIZE
        pred_tag_ids, prob_paths = model.predict_and_get_prob(logits, weight) 

        for idx, (a_pred_seq, a_seq_pred_prob) in enumerate( zip(pred_tag_ids, list(prob_paths) )):
            best_class_prob = a_seq_pred_prob # Not Available step-wise prob. in CRF mode. Just use sequence prob for now
            items = []
            for best_class_index in a_pred_seq: # a_sent : [num_steps, num_target_class]
                item = "{},{}".format(best_class_index, best_class_prob)
                items.append( item ) 
            a_line = "\t".join( items )
            result_lines.append( a_line )

    with codecs.open(to_result_fn, 'w', encoding='utf-8') as f:
        for line in result_lines:
            print(line, file=f)

    print("Prediction result is dumped at {}".format(to_result_fn) )


# ------- for serving ----------------------- #
from domains.common.rsc_processor.n21 import convert_a_sent_to_n21_example
class Predictor_N21:
    def __init__(self, model_fn, token_vocab, class_vocab, device='gpu'):
        self.model = load_model(model_fn, map_location=device ) 
        self.token_vocab = token_vocab 
        self.class_vocab = class_vocab
        self.r_class_vocab = { v: k for k,v in class_vocab.items() } # reverse vocab
        self.device = torch.device(device)
        
        self.batch_size = 1

    def predict(self, text):
        # convert text to id
        a_ex = convert_a_sent_to_n21_example(text, self.token_vocab, self.model.hps.num_steps )

        token_ids = torch.tensor([a_ex.token_ids], dtype=torch.long)
        weight    = torch.tensor([a_ex.weight],    dtype=torch.float)

        # forward pass
        predicts  = self.model(token_ids, weight, mode='eval')
        np_probs  = F.softmax(predicts, dim=1).data.cpu().numpy() # to get probability
        pred_idxs = np.argmax(np_probs, axis=1)

        # convert id to symbol
        pred_class_id = pred_idxs[0]
        predicted_class = self.r_class_vocab[pred_class_id]
        predicted_prob  = np_probs[0][pred_class_id]

        return predicted_class, predicted_prob


from domains.common.rsc_processor.n2n import convert_a_sent_to_n2n_example
class Predictor_N2N:
    def __init__(self, model_fn, token_vocab, class_vocab, pad_token_id, o_tag_id, device='gpu'):
        self.model = load_model(model_fn, map_location=device ) 
        self.token_vocab = token_vocab 
        self.class_vocab = class_vocab
        self.r_class_vocab = { v: k for k,v in class_vocab.items() } # reverse vocab
        self.device = torch.device(device)
        
        self.pad_token_id = pad_token_id
        self.o_tag_id = o_tag_id

        self.batch_size = 1

    def predict(self, text):
        # convert text to id
        a_ex = convert_a_sent_to_n2n_example(text, self.token_vocab, self.model.hps.num_steps, self.pad_token_id, self.o_tag_id)
        
        token_ids = torch.tensor([a_ex.token_ids], dtype=torch.long)
        weight    = torch.tensor([a_ex.weight],    dtype=torch.float)
        tag_ids   = torch.tensor([a_ex.tag_ids],   dtype=torch.long)

        # feed-forward (inference)
        logits = self.model(token_ids)  # logits = [batch_size, num_steps, num_target_class]

        # pred_tag_ids : a list of list ( list for masked tag ids ) <- BATCH_SIZE
        # prob_paths   : a numpy list path prob <- BATCH_SIZE
        b_pred_tag_ids, b_prob_paths = self.model.predict_and_get_prob(logits, weight) 

        # take the first (since predict function handle single sentence at a time)
        a_pred_tag_ids = b_pred_tag_ids[0] 
        predicted_seq_prob = b_prob_paths[0]

        predicted_slots = []
        predicted_slot_prob = []

        for best_class_index in a_pred_tag_ids:
            best_class_prob = predicted_seq_prob # Not Available step-wise prob. in CRF mode. Just use sequence prob for now
            best_class = self.r_class_vocab[best_class_index]

            predicted_slots.append(best_class)
            predicted_slot_prob.append(best_class_prob)

        return predicted_slots, predicted_slot_prob, predicted_seq_prob
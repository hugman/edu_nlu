"""
    GPU realted Utils (Common)

    Author : Sangkeun Jung (hugmanskj@gmail.com), 2019
"""
import torch 
def is_gpu_available():
    return torch.cuda.is_available()


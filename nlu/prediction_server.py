"""
    Serving NLU Prediction 
        - Intent Analysis
        - Entity Analysis

        with text line input mode
        with rest api mode


    Author : Sangkeun Jung (hugmanskj@gmail.com), 2019
"""
import os 


# set current SCRIPT file directory as a working directory
os.chdir( os.path.dirname( os.path.abspath(__file__) ) )

# --------------------------------------------------------------------------------------
from domains.common.utils.predict import Predictor_N21, Predictor_N2N
from domains.common.rsc_processor.vocabs import load_vocabs
from domains.common.utils.gpu import is_gpu_available

import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--model_key", help=" model identifier", default="default")
parser.add_argument("--use_gpu", help="use gpu=True or False", default=False)
#parser.add_argument("--use_gpu", help="use gpu=True or False", default=True)
parser.add_argument("--gpu_id", help="gpu device id", default="0")

parser.add_argument("--domain", help="domain name", default="weather")             # dm

args = parser.parse_args()
os.environ ["CUDA_VISIBLE_DEVICES"] = args.gpu_id

if not is_gpu_available(): args.use_gpu = False

import sys; 
nlu_predictor = None 

def prepare_predictor(fn_model, vocabs, device='cpu'):
    global nlu_predictor

    # return predictor
    # load vocabs
    try:
        vocabs = load_vocabs(fns['vocab']) # key = token or n21 or n2n 
        pad_token_id = vocabs['token']['_PAD']
        o_tag_id=vocabs['n2n']['O']
    except:
        print("Error at vocabulary loading")
        sys.exit()

    # load intent model 
    try:
        intent_predictor = Predictor_N21(fn_model['n21'], vocabs['token'], vocabs['n21'], device)
        print("Intent Predictor is succefully loaded")
    except:
        print("Error at loading intent predictor")
        sys.exit()

    # load entity model 
    try:
        slot_predictor = Predictor_N2N(fn_model['n2n'], vocabs['token'], vocabs['n2n'], pad_token_id, o_tag_id, device)
        print("Slot Predictor is succefully loaded")
    except:
        print("Error at loading slot predictor")
        sys.exit()

    predictors = (intent_predictor, slot_predictor)
    global nlu_predictor
    nlu_predictor = predictors
    return predictors

import time
def do_nlu(text):
    import json 
    global nlu_predictor
    intent_predictor, slot_predictor = nlu_predictor
    
    s_time = time.time()
    # intent
    predicted_intent, predicted_prob = intent_predictor.predict(text)
    #print("\t[{} \t {:5.3f}]".format(predicted_intent, predicted_prob ))

    # slot
    predicted_slots, predicted_slot_prob, predicted_seq_prob = slot_predictor.predict(text)
    #print("\t[{:5.3f}] \t".format(predicted_seq_prob), predicted_slots)
    e_time = time.time()

    elapsed_time = e_time - s_time

    # make json data
    result = {
                'meta': {
                            'domain' : domain,
                        },
                'text' : text, # we assume single sentence NLU
                'nlu' : {
                            'intent' : [ # for multiple intents -- use list
                                        {
                                            'tag' : predicted_intent,
                                            'probability' : float(predicted_prob)
                                        },
                                        ],
                            'slot'   : {
                                            'probability' : float(predicted_seq_prob), # sequence-wise predicted prob.
                                            'tags' : predicted_slots        # a list of tags
                                        },
                            'elapsed_time': elapsed_time,
                }
                }
    result_json = json.dumps(result, indent=4, ensure_ascii=False)
    return result_json

def run_text_server(predictors, domain):
    import json 
    print("Server is ready")
    intent_predictor, slot_predictor = predictors
    while True:
        text = input('Type sentence : ')
        result_json = do_nlu(text) 
        print(result_json)

# WEB SERVER RELATED SETTINGS -------------------------------------------------------
from os import environ
from flask import Flask
app = Flask(__name__)
from flask import request

@app.route('/nlu')
def nlu():
    text = request.args.get('text')
    result_json = do_nlu(text)
    return result_json
# -----------------------------------------------------------------------------------


def run_web_server(predictors, domain):
    # run web server --------------------------------------------------
    HOST = environ.get('SERVER_HOST', 'localhost')
    #HOST = environ.get('SERVER_HOST', '0.0.0.0')
    try:
        PORT = int(environ.get('SERVER_PORT', '5555'))
    except ValueError:
        PORT = 5555
    #app.run(HOST, PORT, debug=True)
    app.run(HOST, PORT, debug=False)



if __name__ == '__main__':
    domain = args.domain 

    data_root = '../data'
    model_root = '../model'

    text_data_folder     = os.path.join(data_root, domain, "tokenized")
    id_data_folder       = os.path.join(data_root, domain, "id_converted")
    vocab_folder         = os.path.join(data_root, domain, "vocab")

    model_dir       = os.path.join(model_root, domain, args.model_key)

    fns = {
            'vocab' : {
                        'token' : os.path.join(vocab_folder, 'token.vocab'),
                        'n21' : os.path.join(vocab_folder, 'n21.class.voab'),
                        'n2n' : os.path.join(vocab_folder, 'n2n.class.voab'),
                },
            'model' : {
                        'n21' : os.path.join(model_dir, 'model.n21.out'),
                        'n2n' : os.path.join(model_dir, 'model.n2n.out'),
                    }
          }

    predictors = prepare_predictor(fns['model'], fns['vocab'], device='cpu')
    #predictors = prepare_predictor(fns['model'], fns['vocab'])

    # TEXT 
    #run_text_server(predictors, domain)

    # WEB 
    # ex) http://localhost:5555/nlu?text=오늘 날씨 어떻게 되지?
    run_web_server(predictors, domain)
    




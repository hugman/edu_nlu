"""
    Train Natural Language Understanding (NLU)
        - Domain Entity Analysis

    Author : Sangkeun Jung (hugmanskj@gmail.com), 2019
"""


# in this code, 
#   intent alaysis and domain_entity analysis implemented seperatley. 
#   (NOT JOINT MODEL for education purpose )
import os, codecs
from common.ml.hparams import HParams

# set current SCRIPT file directory as a working directory
os.chdir( os.path.dirname( os.path.abspath(__file__) ) )

# for training env. specific things ------------------------------------------------------
from domains.common.utils.gpu import is_gpu_available

# for domain specific things ------------------------------------------------------
from domains.common.rsc_processor.n2n import load_rsc, make_examples
from domains.common.rsc_processor.n2n import load_text_rsc, load_n2n_predicted_result_format
from domains.common.rsc_processor.vocabs import load_vocabs
from domains.common.utils.predict import load_model

# -- for model speicific things - crf
#from domains.common.network.nn import lstm_n2n as network
#from domains.common.utils.train import train_n2n as train
#from domains.common.utils.predict import test_n2n as test

from domains.common.network.nn import lstm_crf_n2n as network
from domains.common.utils.train import train_crf_n2n as train
from domains.common.utils.predict import test_crf_n2n as test
# -----------------



def run_train_domain_entity(train_fn, hps, model_fn, pad_token_id=1, o_tag_id=0):
    # run nlu trainer (domain entity)
    # input : converted id format (train.id.n2n.txt)
    # output : model file 
    
    train_rsc = load_rsc(train_fn)
    train_exs = make_examples(train_rsc, hps.num_steps)
    model = network(hps)
    train(model, train_exs, hps, model_fn)


def run_eval_domain_entity(test_fn, model_fn, to_result_fn, pad_token_id=1, o_tag_id=0):
    # run nlu evaluator (domain entity)
    model = load_model(model_fn) 

    test_rsc = load_rsc(test_fn)
    test_exs = make_examples(test_rsc, model.hps.num_steps)
    
    fast_eval = False # True? fast evaluation (not correct probability)
    test(model, test_exs, to_result_fn, fast_eval)


def convert_result_format(text_n2n_fn, n2n_result_fn, text_n2n_result_fn, vocab):
    # input : 
    #   - text tokenized n2n format, 
    #   - n2n_result_fn(only id and probability) (predicted), 
    #   - readable n2n text format (predicted)
    #   - vocab : n2n class symbol vocab

    reverse_vocab = { v: k for k, v in vocab.items() } 

    text_rsc = load_text_rsc(text_n2n_fn)
    predicted_rsc = load_n2n_predicted_result_format(n2n_result_fn)

    if len(text_rsc) != len(predicted_rsc):
        print("ERROR: Reference({}) and Predicted({}) results do not match!!".format(len(text_rsc), len(predicted_rsc)) )
        import sys; sys.exit()

    # store the readable output (text_n2n_result_fn)
    conv_result = []
    for sent_idx, (text_sent, id_sent) in enumerate( zip(text_rsc, predicted_rsc) ):

        if len(text_sent) != len(id_sent):
            print("ERROR: Sentence lengths do not match at sentence {} : Reference({}) vs. Predicted({})".format(sent_idx, len(text_sent), len(id_sent)) )
            import sys; sys.exit()


        
        a_sent = []
        probs = []
        for item in zip(text_sent, id_sent):
            token, ref_class = item[0]
            pred_class_id, pred_prob = item[1]
            pred_class = reverse_vocab[pred_class_id]

            _diff = 'O' if ref_class == pred_class else 'X' 
            probs.append( pred_prob ) 
            a_sent.append( (_diff, token, ref_class, pred_class, pred_prob) )

        sent_prob = sum(probs) / len( probs ) 
        conv_result.append( (a_sent, sent_prob))

    # dump
    with codecs.open(text_n2n_result_fn, 'w', encoding='utf-8') as f:
        for sent_number, (a_sent, sent_prob) in enumerate( conv_result ):
            for _diff, token, ref_class, pred_class, pred_prob in a_sent:
                print("[{}]\t{}\t{}\t{}\t{:5.3f}".format(
                                                    _diff,
                                                    token,
                                                    ref_class,
                                                    pred_class,
                                                    pred_prob
                                                ),
                      file=f
                      )
            print("{} [{}]>>> {}".format("-"*50, sent_prob, sent_number+1), file=f)
        print("Readable N2N result format is dumped at {}".format(text_n2n_result_fn) )


def performance_eval(fn):
    # file should be readable n2n predicted format 
    #

    from common.nlp.measure.get_performance import get_performance

    # store (token, ref_target, predicted_target) for n2n evaluation 

    data = []
    with codecs.open(fn, 'r', encoding='utf-8') as f:
        a_sent = []
        for line in f:
            line = line.rstrip()

            if line.startswith('-'*20): 
                # end of sentence
                data.append( a_sent ) 
                a_sent = []
                continue 

            field = line.split('\t')
            _diff, token, ref_class, pred_class, pred_prob = field
            a_sent.append( (token, ref_class, pred_class) )

    # eval 
    perf = get_performance(data)
    print(" Precision : {} \t Recall : {} \t F-Measure : {}".format(perf['precision'], 
                                                                    perf['recall'],
                                                                    perf['FB1']
                                                                    ))


import argparse
parser = argparse.ArgumentParser()
parser.add_argument("--mode", help="mode - train, eval, pred", default="train")
#parser.add_argument("--mode", help="mode - train, eval, pred", default="eval")
#parser.add_argument("--mode", help="mode - train, eval, pred", default="pred")


parser.add_argument("--gpu_id", help="gpu device id", default="0")
parser.add_argument("--model_key", help="model identifier", default="default")

#parser.add_argument("--use_gpu", help="use gpu=True or False", default=True)
parser.add_argument("--use_gpu", help="use gpu=True or False", default=False)


parser.add_argument("--keep_prob", help="keep prob. for dropout", default=0.5)           # Kp
parser.add_argument("--token_emb_dim", help="token_emb_dim", default=50, type=int)       # T=
parser.add_argument("--slot_dim", help="slot_dim", default=50, type=int)             # Sd=

parser.add_argument("--domain", help="domain name", default="weather")             # dm=


args = parser.parse_args()
os.environ["CUDA_VISIBLE_DEVICES"] = args.gpu_id


if __name__ == '__main__':
    domain = args.domain 
    
    data_root = '../data'
    model_root = '../model'

    text_data_folder     = os.path.join(data_root, domain, "tokenized")
    id_data_folder       = os.path.join(data_root, domain, "id_converted")
    vocab_folder         = os.path.join(data_root, domain, "vocab")

    model_dir       = os.path.join(model_root, domain, args.model_key)

    fns = {
            'train' : os.path.join(id_data_folder, 'train.id.n2n.txt'),
            'test'  : os.path.join(id_data_folder, 'test.id.n2n.txt'),
            'vocab' : {
                        'token' : os.path.join(vocab_folder, 'token.vocab'),
                        'n21' : os.path.join(vocab_folder, 'n21.class.voab'),
                        'n2n' : os.path.join(vocab_folder, 'n2n.class.voab'),
                },
            'model' : os.path.join(model_dir, 'model.n2n.out'),
            'text' : { # to dump result form
                        'train' : os.path.join(text_data_folder, 'train.text.n2n.txt'),
                        'test'  : os.path.join(text_data_folder, 'test.text.n2n.txt'),
                     },

            'result_fn' : os.path.join(model_dir, 'result.n2n.id.txt'),
            'result_fn_n2n' : os.path.join(model_dir, 'result.n2n.text.txt'),
          }

    vocabs = load_vocabs(fns['vocab']) # key = token or n21 or n2n 
    pad_token_id = vocabs['token']['_PAD']
    o_tag_id=vocabs['n2n']['O']

    if args.mode == 'train':
        hps= HParams(
                            ## common
                            batch_size        = 100,
                            num_steps         = 64,  # max token numbers in a sentence
                            learning_rate     = 0.01,
                            keep_prob         = float(args.keep_prob),  # 1.0 for evaluation & prediction
                            
                            max_epoch         = 50,
                            use_gpu           = args.use_gpu,

                            # encoder
                            token_emb_dim     = args.token_emb_dim,
                            text_encoder_num_layers = 1,
                            slot_dim          =  args.slot_dim,

                            # vocab 
                            num_token_vocabs  = len( vocabs['token'] ),
                            num_slot_tags     = len( vocabs['n2n']),
                        )

        if not os.path.exists(model_dir): os.makedirs(model_dir)

        run_train_domain_entity(fns['train'], hps, fns['model'], pad_token_id, o_tag_id)
        run_eval_domain_entity(fns['test'], fns['model'], fns['result_fn'], pad_token_id, o_tag_id)
        convert_result_format(fns['text']['test'], fns['result_fn'], fns['result_fn_n2n'], vocabs['n2n'])
        performance_eval(fns['result_fn_n2n'])

    if args.mode == 'eval':
        run_eval_domain_entity(fns['test'], fns['model'], fns['result_fn'], pad_token_id, o_tag_id)
        convert_result_format(fns['text']['test'], fns['result_fn'], fns['result_fn_n2n'], vocabs['n2n'])
        performance_eval(fns['result_fn_n2n'])

    
    if args.mode == 'pred':
        from domains.common.utils.predict import Predictor_N2N
        predictor = Predictor_N2N(fns['model'], vocabs['token'], vocabs['n2n'], pad_token_id, o_tag_id, device='cpu')
        predicted_slots, predicted_slot_prob, predicted_seq_prob = predictor.predict("오늘 날씨 어떻게 되지?")
        print(predicted_slots)
        print(predicted_slot_prob)
        print(predicted_seq_prob)

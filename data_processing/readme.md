### Data Processing Flow ###

1. Excel to Raw Text file
	python convert_excel_to_tagged_text.py

2. Raw XML based text to BIO format text (n21, n2n)
	python convert_xml_to_bio.py

3. Make Vocabulary (( Strong Assumption -- we just use character from the data for educational purpose ))
	python collect_vocab.py

4. Convert All text to id based data
	python convert_text_to_id.py


"""
    Convert Excel NLU format to xml-like tagged text format

"""

import os, sys
os.chdir( os.path.dirname( os.path.abspath(__file__ ) ) )


def load_intent_sheet(sheet):
    # parse intent sheet, 
    # return inetnt names and corresponding sheet name

    num_rows = sheet.nrows 
    num_cols = sheet.ncols 
 
    # --- header 
    # 번호	기능명	대표 사용자 문장	시스템 응답	Intent

    intent_sheet_name_dict = {}

    for row_idx in range(num_rows):
        if row_idx == 0: continue  # pass

        corresponding_sheet_name = sheet.cell_value(row_idx, 1) # sheet name
        intent_name = sheet.cell_value(row_idx, 4) # intent name 
        intent_sheet_name_dict[corresponding_sheet_name] = intent_name

    return intent_sheet_name_dict

def get_content_sheets(workbook):
    # input : workbook
    # output : dictionary of content --> key=sheet_name, value=sheet_object
    #
    # we use '||||||||' for sheet seperator
    
    data = {}

    sheet_names = workbook.sheet_names() 
    is_content_sheet = False
    for sheet_name in sheet_names:
        if sheet_name.startswith('|'*5):
            is_content_sheet = True
            continue 

        if is_content_sheet:
            sheet = workbook.sheet_by_name(sheet_name)
            data[ sheet_name ] = sheet
        else:
            continue 

    return data

def validate_intent_and_sheets(sheet_names_a, sheet_names_b):

    a = sorted( sheet_names_a )
    b = sorted( sheet_names_b )

    if a != b :
        print("[ERROR] Sheet names of intent sheet and content sheet are not matched! Check the file.")
        import sys; sys.exit()
    else:
        print("[Passed] Sheet names comparison.")
        

def load_content_sheet(sheet):
    # input : sheet object
    # output : a list of xml like tagged sentence

    num_rows = sheet.nrows 
    num_cols = sheet.ncols 
 
    sents = []

    # --- header 
    # 기능명	시스템 응답	문장	Intent	Annotation
    for row_idx in range(num_rows):
        if row_idx == 0: continue  # pass

        flag = sheet.cell_value(row_idx, 0) 
        text = sheet.cell_value(row_idx, 2) 
        tagged_text = sheet.cell_value(row_idx, 4) 

        if flag.startswith('//'): continue  ## comment process

        item = (text, tagged_text, row_idx)
        sents.append( item ) 

    return sents


def check_xml_validation(content, sheet_name):
    import xml.dom.minidom
    
    all_passed = True 
    for sent_tuple in content:
        text, tagged_text, row_idx = sent_tuple

        # xml use ':' as a reserved tag. Temporarily, change it to pass 
        test_xml = '<SENT>{}</SENT>'.format(tagged_text)
        if ':' in test_xml:
            test_xml = test_xml.replace(':', '_')

        try:
            xml.dom.minidom.parseString(test_xml)
        except:
            print("[ERROR] Not valid at line number {} in {} sheet : {}".format(row_idx+1, sheet_name, tagged_text))
            all_passed = False
    if all_passed:
        print("[Passed] XML-like tag validation ... ", sheet_name)
    else:
        import sys; sys.exit()


def load_excel(fn):
    import xlrd

    workbook = xlrd.open_workbook(fn)
    sheet_names = workbook.sheet_names() 

    function_sheet = workbook.sheet_by_name("functions") # intent sheet 
    intent_sheet_name_dict = load_intent_sheet(function_sheet)

    content_sheets = get_content_sheets(workbook)

    # validate : number of intent and number of corresponding sheet's are same
    validate_intent_and_sheets(intent_sheet_name_dict.keys(), content_sheets.keys() )

    # load sheets and validate
    data = {}
    for sheet_name, sheet in content_sheets.items():
        
        intent = intent_sheet_name_dict[sheet_name]
        content = load_content_sheet(sheet)

        check_xml_validation(content, sheet_name) 
        data[sheet_name] = content

    return data, intent_sheet_name_dict

import codecs
def convert(fn, to_fn):
    # load and validate
    data, intent_sheet_name_dict = load_excel(fn)    

    _dir = os.path.dirname(to_fn)
    if not os.path.exists(_dir): os.makedirs(_dir)

    # dump all data 
    with codecs.open(to_fn, 'w', encoding='utf-8') as f:
        for sheet_name, content in data.items():
            intent = intent_sheet_name_dict[sheet_name]
            
            for text, tagged_text, row_idx in content:
                if not tagged_text: continue # empty? pass
                print("{}\t{}".format(intent, tagged_text), file=f)
    print()
    print("Converted text file is dumped at {}".format(to_fn))

if __name__ == '__main__':
    
    fn = '../data/weather/excel/2019.04.14_weather.xlsx'
    to_fn = '../data/weather/raw/raw.txt'

    convert(fn, to_fn)





